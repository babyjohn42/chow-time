package com.babyjohn42.chowtime

import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.view.HapticFeedbackConstants
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.babyjohn42.chowtime.data.Feeding
import com.babyjohn42.chowtime.databinding.ActivityMainBinding
import com.babyjohn42.chowtime.viewmodels.ChowTimeViewModel
import com.babyjohn42.chowtime.widget.WidgetIntentUtility
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val chowTimeViewModel: ChowTimeViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        chowTimeViewModel.deleteOldFeedings()

        chowTimeViewModel.lastFeeding.observe(this) { feeding -> onLastFeedingChange(feeding) }

        setDogImage()
        binding.activityMainImageDog.setOnClickListener { feed() }
        binding.activityMainImageDog.setOnLongClickListener { undoFeeding() }

        val view = binding.root
        setContentView(view)
    }

    private fun onLastFeedingChange(feeding: Feeding?) {
        if (feeding != null) {
            val formatter = SimpleDateFormat("hh:mm aaa")
            binding.activityMainLastFeedingTime.text = getString(R.string.activity_main_last_feeding_time_text, formatter.format(feeding.feedingDateTime.time))
        } else {
            binding.activityMainLastFeedingTime.text = ""
        }

        val animation = binding.activityMainImageDog.drawable as? AnimationDrawable
        if (animation?.isRunning != true) {
            setDogImage()
        }

        setBackgroundColor(feeding != null)
    }

    private fun feed() {
        binding.activityMainImageDog.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP)

        if (chowTimeViewModel.hasBeenFedRecently()) {
            Toast.makeText(this, "You've already fed me!", Toast.LENGTH_SHORT).show()
        } else {
            setDogAnimation()
            chowTimeViewModel.addFeeding()
            WidgetIntentUtility.updateWidget(this)
        }
    }

    private fun undoFeeding() : Boolean {
        binding.activityMainImageDog.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

        if (!chowTimeViewModel.hasBeenFedRecently()) {
            Toast.makeText(this, "Can't undo, too much time has passed", Toast.LENGTH_SHORT).show()
        } else {
            if (chowTimeViewModel.undoFeeding()) {
                setDogImage()
                Toast.makeText(this, "Last feeding removed", Toast.LENGTH_SHORT).show()
                WidgetIntentUtility.updateWidget(this)
            }
        }

        return true
    }

    private fun setDogImage() {
        val numberOfFeedings = chowTimeViewModel.numberOfFeedings()
        binding.activityMainImageDog.setImageResource(
            when (numberOfFeedings) {
                0 -> R.drawable.image_dog_neutral_0
                1 -> R.drawable.image_dog_neutral_1
                2 -> R.drawable.image_dog_neutral_2
                3 -> R.drawable.image_dog_neutral_3
                else -> throw IllegalStateException("Should never have more than 3 feedings")
            }
        )
    }

    private fun setBackgroundColor(shouldShow: Boolean) {
        binding.activityMainLastFeedingTime.setBackgroundColor(
            if (shouldShow) {
                ContextCompat.getColor(this, R.color.transparent_black)
            } else {
                Color.TRANSPARENT
            })
    }

    private fun setDogAnimation() {
        binding.activityMainImageDog.setImageResource(
            when (chowTimeViewModel.numberOfFeedings()) {
                0 -> R.drawable.anim_dog_fed_1
                1 -> R.drawable.anim_dog_fed_2
                2 -> R.drawable.anim_dog_fed_3
                3 -> R.drawable.anim_dog_full
                else -> throw IllegalStateException("Should never have more than 3 feedings")
            }
        )

        val animation = binding.activityMainImageDog.drawable as AnimationDrawable
        animation.start()
    }
}
