package com.babyjohn42.chowtime.widget

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Build

class WidgetIntentUtility {
    companion object {
        const val onBowlClick = "onBowlClickTag"

        @SuppressLint("UnspecifiedImmutableFlag")
        fun getPendingSelfIntent(context: Context, appWidgetId: Int, widgetClass: Class<*>) : PendingIntent {
            val intent = Intent(context, widgetClass)
            intent.action = onBowlClick
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                PendingIntent.getBroadcast(context, appWidgetId, intent, PendingIntent.FLAG_IMMUTABLE)
            } else {
                PendingIntent.getBroadcast(context, appWidgetId, intent, 0)
            }
        }

        fun updateWidget(context: Context) {
            updateWidget(context, BowlAppWidget::class.java)
            updateWidget(context, DogAppWidget::class.java)
        }

        private fun updateWidget(context: Context, widgetClass: Class<*>) {
            val intent = Intent(context, widgetClass)
            intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            val appWidgetManager = AppWidgetManager.getInstance(context)
            val ids = appWidgetManager.getAppWidgetIds(ComponentName(context, widgetClass))
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
            context.sendBroadcast(intent)
        }
    }
}