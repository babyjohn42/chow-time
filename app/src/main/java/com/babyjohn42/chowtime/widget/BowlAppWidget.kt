package com.babyjohn42.chowtime.widget

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import com.babyjohn42.chowtime.R
import com.babyjohn42.chowtime.data.FeedingRepository
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.runBlocking
import java.text.SimpleDateFormat
import java.util.Calendar
import javax.inject.Inject

@AndroidEntryPoint
class BowlAppWidget : AppWidgetProvider() {
    @Inject lateinit var feedingRepository: FeedingRepository

    override fun onEnabled(context: Context) {
        super.onEnabled(context)

        WidgetIntentUtility.updateWidget(context)
    }

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)

        if (intent.action == WidgetIntentUtility.onBowlClick) {
            feed()
            WidgetIntentUtility.updateWidget(context)
        }
    }

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        appWidgetIds.forEach { appWidgetId ->
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    private fun updateAppWidget(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetId: Int
    ) {
        val feeding = runBlocking {
            feedingRepository.getLastFeeding().firstOrNull()
        }

        val numberOfFeedings = runBlocking {
            feedingRepository.getTodaysFeedings().firstOrNull()?.size ?: 0
        }

        val views = RemoteViews(
            context.packageName,
            R.layout.app_widget_bowl
        ).apply {

            val pendingIntent = WidgetIntentUtility.getPendingSelfIntent(context, appWidgetId, BowlAppWidget::class.java)
            setOnClickPendingIntent(R.id.widget_bowl_image, pendingIntent)
            setOnClickPendingIntent(R.id.widget_bowl_time, pendingIntent)

            if (feeding == null) {
                setTextViewText(R.id.widget_bowl_time, "")
            } else {
                val formatter = SimpleDateFormat("hh:mm aaa")
                setTextViewText(R.id.widget_bowl_time, formatter.format(feeding.feedingDateTime.time))
            }

            setImageViewResource(
                R.id.widget_bowl_image, when (numberOfFeedings) {
                    0 -> R.drawable.image_bowl_0
                    1 -> R.drawable.image_bowl_1
                    2 -> R.drawable.image_bowl_2
                    3 -> R.drawable.image_bowl_3
                    else -> throw IllegalStateException("Should never have more than 3 feedings")
                }
            )
        }

        appWidgetManager.updateAppWidget(appWidgetId, views)
    }

    private fun feed() {
        runBlocking {
            val lastFeeding = feedingRepository.getLastFeeding().firstOrNull()?.feedingDateTime
            lastFeeding?.roll(Calendar.HOUR, 1)
            val hasBeenFeedRecently = lastFeeding?.after(Calendar.getInstance()) ?: false
            val numberOfFeedings = feedingRepository.getTodaysFeedings().firstOrNull()?.size ?: 0
            if (!hasBeenFeedRecently && numberOfFeedings < 3) {
                feedingRepository.createFeeding()
            }
        }
    }
}