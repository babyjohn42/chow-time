package com.babyjohn42.chowtime.di

import android.content.Context
import com.babyjohn42.chowtime.data.AppDatabase
import com.babyjohn42.chowtime.data.FeedingDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context) : AppDatabase {
        return AppDatabase.getInstance(context)
    }

    @Provides
    fun provideFeedingDao(appDatabase: AppDatabase) : FeedingDao {
        return appDatabase.feedingDao()
    }
}