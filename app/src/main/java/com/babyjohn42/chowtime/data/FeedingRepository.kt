package com.babyjohn42.chowtime.data

import kotlinx.coroutines.flow.Flow
import java.util.Calendar
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FeedingRepository @Inject constructor(
    private val feedingDao: FeedingDao
) {

    suspend fun createFeeding() {
        feedingDao.insertFeeding(Feeding())
    }

    suspend fun deleteOldFeedings() {
        val weekAgo = Calendar.getInstance()
        weekAgo.add(Calendar.DAY_OF_YEAR, -7)
        feedingDao.deleteFeedingsBeforeDateTime(weekAgo)
    }

    fun getTodaysFeedings(): Flow<List<Feeding>> {
        val startOfToday = Calendar.getInstance()
        startOfToday.set(Calendar.HOUR_OF_DAY, 0)
        startOfToday.set(Calendar.MINUTE, 0)
        startOfToday.set(Calendar.SECOND, 0)
        startOfToday.set(Calendar.MILLISECOND, 0)

        return feedingDao.getFeedingsAfterDateTime(startOfToday)
    }

    fun getLastFeeding(): Flow<Feeding> {
        return feedingDao.getLastFeeding()
    }

    suspend fun deleteFeeding(feeding: Feeding) {
        feedingDao.deleteFeeding(feeding)
    }
}