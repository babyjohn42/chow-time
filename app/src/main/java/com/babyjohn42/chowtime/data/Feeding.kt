package com.babyjohn42.chowtime.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Calendar

@Entity(
    tableName = "feedings"
)
data class Feeding(
    @ColumnInfo(name = "feeding_date_time") val feedingDateTime: Calendar = Calendar.getInstance()
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var feedingId: Int = 0
}