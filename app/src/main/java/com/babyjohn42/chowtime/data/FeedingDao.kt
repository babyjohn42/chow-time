package com.babyjohn42.chowtime.data

import androidx.room.*
import java.util.Calendar
import kotlinx.coroutines.flow.Flow

@Dao
interface FeedingDao {
    @Query("SELECT * FROM feedings WHERE feeding_date_time > :dateTime")
    fun getFeedingsAfterDateTime(dateTime: Calendar): Flow<List<Feeding>>

    @Query("SELECT * FROM feedings ORDER BY feeding_date_time DESC LIMIT 1")
    fun getLastFeeding(): Flow<Feeding>

    @Transaction
    @Query("DELETE FROM feedings WHERE feeding_date_time < :dateTime")
    suspend fun deleteFeedingsBeforeDateTime(dateTime: Calendar)

    @Insert
    suspend fun insertFeeding(feeding: Feeding)

    @Delete
    suspend fun deleteFeeding(feeding: Feeding)
}