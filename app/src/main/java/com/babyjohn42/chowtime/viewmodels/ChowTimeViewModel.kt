package com.babyjohn42.chowtime.viewmodels

import androidx.lifecycle.asLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.babyjohn42.chowtime.data.FeedingRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.Calendar
import javax.inject.Inject

@HiltViewModel
class ChowTimeViewModel @Inject constructor(
    private val feedingRepository: FeedingRepository
) : ViewModel() {

    val lastFeeding = feedingRepository.getLastFeeding().asLiveData()

    fun numberOfFeedings() : Int {
        return runBlocking {
            feedingRepository.getTodaysFeedings().firstOrNull()?.size ?: 0
        }
    }

    fun deleteOldFeedings() {
        viewModelScope.launch {
            feedingRepository.deleteOldFeedings()
        }
    }

    fun hasBeenFedRecently() : Boolean {
        return runBlocking {
            val lastFeeding = feedingRepository.getLastFeeding().firstOrNull()?.feedingDateTime
            lastFeeding?.roll(Calendar.HOUR, 1)
            lastFeeding?.after(Calendar.getInstance()) ?: false
        }
    }

    fun addFeeding() {
        viewModelScope.launch {
            if (numberOfFeedings() < 3) {
                feedingRepository.createFeeding()
            }
        }
    }

    fun undoFeeding() : Boolean {
        val canUndo = runBlocking {
            feedingRepository.getTodaysFeedings().firstOrNull()?.contains(feedingRepository.getLastFeeding().firstOrNull()) ?: false
        }
        if (canUndo) {
            viewModelScope.launch {
                feedingRepository.deleteFeeding(lastFeeding.value!!)
            }
        }
        return canUndo
    }
}